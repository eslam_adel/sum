package com.migamind.sum;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private EditText FirstNumber;
    private EditText SecondeNumber;
    private EditText Result;
    private Button button;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FirstNumber = findViewById(R.id.FirstNumber);
        SecondeNumber = findViewById(R.id.SecondeNumber);
        Result = findViewById(R.id.Result);
        button = findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int n1 = Integer.parseInt(FirstNumber.getText().toString());
                int n2 = Integer.parseInt(SecondeNumber.getText().toString());
                int r = n1 + n2;
                Result.setText(String.valueOf(r));
            }
        });


    }
}
